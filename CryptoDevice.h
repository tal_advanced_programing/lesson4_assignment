#pragma once

#include <cstdio>
#include <iostream>
#include <string.h>
#include <cstdlib>
#include "osrng.h"
#include "modes.h"
#include <hex.h>

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <md5.h>

using namespace CryptoPP;

class CryptoDevice
{

public:
    std::string encryptAES(std::string);
    std::string decryptAES(std::string);

	std::string encryptMD5(std::string message);


private:
    byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];

};
